package com.utbm.activity_tracker.ui.activities;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.semantic.ecare_android_v2.R;
import com.semantic.ecare_android_v2.ui.MeasureActivity;
import com.semantic.ecare_android_v2.ui.MeasureSetPatientActivity;
import com.semantic.ecare_android_v2.ui.common.activity.GenericConnectedActivity;
import com.utbm.activity_tracker.core.model.ActivityTracker;
import com.utbm.activity_tracker.core.service.ActivityTrackerBLEService;
import com.semantic.ecare_android_v2.object.CompoundMeasure;
import com.semantic.ecare_android_v2.util.Constants;
import com.utbm.activity_tracker.core.repository.ActivityTrackerRepository;
import com.utbm.activity_tracker.core.model.BleUserInfo;
import com.utbm.activity_tracker.core.service.BluetoothLeService;
import com.utbm.activity_tracker.ui.dialog.SetPatientActivityTrackerDialogFragment;
import com.utbm.activity_tracker.ui.viewcontroller.AlertViewController;
import com.utbm.activity_tracker.ui.viewcontroller.PatientDataController;

import net.newel.android.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Julien on 16/04/16.
 */
public class PodometerSettingActivity extends GenericConnectedActivity implements SetPatientActivityTrackerDialogFragment.OnDialogFragmentClickListener {

    /**
     * Class name
     */
    private String CLASSNAME = this.getClass().getName();

    public final String AT_STATUS_CONNEXION_PENDING    = "Connexion au podomètre en cours";
    public final String AT_STATUS_CONNEXION_ERROR      = "Echec de la connexion au podometre";
    public final String AT_STATUS_CONNECTED            = "Podomètre connecté";
    public final String AT_STATUS_DISCONNECTED         = "Podomètre déconnecté";
    public final String AT_STATUS_READING_DATA         = "Lecture les informations en cours";
    public final String AT_STATUS_READING_DATA_DONE    = "Fin de lecture des informations";
    public final String AT_STATUS_CHECKING_NEW_DATA    = "Vérification des nouvelles données";
    public final String AT_STATUS_WRITING_DATA         = "Enregistrement des nouvelles données";
    public final String AT_STATUS_SYNC_SUCCESS         = "Synchronisation effectuée avec succès";

    private ActivityTrackerBLEService mBluetoothLeService;
    private ActivityTracker activityTracker;

    private boolean mConnected = false;
    private boolean mScanning = false;
    private final SimpleDateFormat sdf = new SimpleDateFormat("d M yyyy");

    private AlertViewController alertViewController;
    private PatientDataController patientDataController;


    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

                mConnected = true;
                alertViewController.setText(AT_STATUS_CONNECTED);
                //mScanning = true;
                invalidateOptionsMenu();

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {

                mConnected = false;
                alertViewController.setText(AT_STATUS_DISCONNECTED);
                mScanning = false;
                invalidateOptionsMenu();

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

                byte[] values = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);

                // Updating user informations on the view
                if(values[0] == (byte) 0x42) {
                    // Parse user informations
                    patientDataController.setPatientInfos(new BleUserInfo(values));
                }

                if(values[0] == (byte) 0x02) {
                    Toast.makeText(PodometerSettingActivity.this, "Enregistrement des informations effectué", Toast.LENGTH_SHORT).show();
                    patientDataController.updateView();
                }

                if(values[0] == (byte) 0x82) {
                    Toast.makeText(PodometerSettingActivity.this, "Echec lors de l'enregistrement des informations", Toast.LENGTH_SHORT).show();
                }



//                final Date date = ActivityTrackerParser.parseDateFromBytes(values);

//                if(date != null) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            alertViewController.setText("Récupération de l'activité : " + sdf.format(date));
//                        }
//                    });
//                }

            } else if (BluetoothLeService.ACTION_READING_DATA.equals(action)) {

                if(intent.getStringExtra("extraString") != null) {
                    alertViewController.setText(AT_STATUS_READING_DATA + " " + intent.getStringExtra("extraString"));
                }
//                alertViewController.setLoading(true);


            } else if (BluetoothLeService.ACTION_READING_DATA_DONE.equals(action)) {

                alertViewController.setText(AT_STATUS_READING_DATA_DONE);

            } else if (BluetoothLeService.ACTION_CHECKING_NEW_DATA.equals(action)) {

                alertViewController.setText(AT_STATUS_CHECKING_NEW_DATA);

            } else if (BluetoothLeService.ACTION_WRITING_DATA.equals(action)) {

                alertViewController.setText(AT_STATUS_WRITING_DATA);

            }else if (BluetoothLeService.ACTION_SYNC_SUCCESS.equals(action)) {

                alertViewController.setText(AT_STATUS_SYNC_SUCCESS);
//                alertViewController.setLoading(false);

                patientDataController.updateTvDateLastSync(mBoundService.getSelectedPatient());

                mScanning = false;
                invalidateOptionsMenu();

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                try {
                    Thread.sleep(2000);

//                    mBoundService.requestActiveNotifications();
//                    mBoundService.requestUserInfos();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.TAG_AC, CLASSNAME + " Oncreate Class");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
//        wl.acquire();



        // Lancement du service
        //


    }


    @Override
    protected void affichage_before_binding() {
        setContentView(R.layout.activity_get_tracker_activity);
    }

    /**
     * Initialisation des vues de l'activity
     */
    protected void initViews() {

        // Initialisation du controller permettant de gérer l'alerte info qui diffusera les messages d'infos
        this.alertViewController = new AlertViewController(this, R.id.lNotice, R.id.tvNotice);
        this.alertViewController.setAlertVisible(true);
//        this.alertViewController.setLoading(true);

        // Initialisation du controller permettant de d'afficher les informations du patient, taille du pas, podomètre en cours, date dernière mesure
        this.patientDataController = new PatientDataController(this);
        patientDataController.updateTvDateLastSync(mBoundService.getSelectedPatient());

        // Initialisation du bouton permettant de définir un nouveau podomètre au patient courant
        Button bnPatientActivityTrackerEdit = (Button) findViewById(R.id.bnPatientActivityTrackerEdit);
        bnPatientActivityTrackerEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // On prend soin de déconnecter le tracker en cours, sinon il ne s'affichera pas dans la liste des appareils disponibles
                mBoundService.getActivityTrackerBLEService().disconnectActivityTracker();

                // On affiche un dialog qui va scanner les trackers bluetooth aux alentours
                SetPatientActivityTrackerDialogFragment generalDialogFragment = SetPatientActivityTrackerDialogFragment.newInstance("Associer un podomètre");
                generalDialogFragment.show(getFragmentManager(), "dialog");

            }
        });

        // Création de l'activity tracker à partir du device
        activityTracker = this.mBoundService.getSelectedPatient().getPodometre();
        patientDataController.updateTvActivityTracker(activityTracker);

    }

    /**
     *
     */
    protected void affichage() {
        this.displayPatient();
        this.initViews();

        // Déconnexion du tracker
        mBoundService.getActivityTrackerBLEService().disconnectActivityTracker();

        requestPatientInfos();

    }

    @Override
    protected void newMeasureReceived(ArrayList<CompoundMeasure> mesures){
        //new measure comes without selected patient
        Log.i(Constants.TAG_AC, CLASSNAME + " Reception d'une mesure");
        Intent i = null;
        if(mBoundService.isSaveMeasure())
        {
            i = new Intent(getApplicationContext(), MeasureSetPatientActivity.class);
            i.putExtra("mesures", mesures);
            startActivity(i);
            this.finish();
        }
        else
        {
            Log.i(Constants.TAG_AC, CLASSNAME +  "ok");
            i = new Intent(getApplicationContext(), MeasureActivity.class);
            i.putExtra("mesures", mesures);
            startActivity(i);
            this.finish();
        }
    }

    @Override
    protected void onKeyBack() {
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, BluetoothLeService.makeGattUpdateIntentFilter());
    }

    /**
     *
     */
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unbindService(mServiceConnection);
        //mBluetoothLeService = null;

        // On relance la mesure automatique
        this.mBoundService.getActivityTrackerBLEService().startTaskMeasurements();
    }

    @Override
    public void onOkClicked(SetPatientActivityTrackerDialogFragment dialog, Bundle bundle) {

        if(bundle != null && bundle.getParcelable("device") instanceof BluetoothDevice) {

            BluetoothDevice device = bundle.getParcelable("device");
            setDeviceToPatient(device);

        } else {
            // Veuillez choisir un device valide dans la liste
        }

    }

    @Override
    public void onCancelClicked(SetPatientActivityTrackerDialogFragment dialog, Bundle bundle) {}

    /**
     * Récupère les informations du bracelet connecté et enregistrement dans la BDD
     * @param device
     */
    private void setDeviceToPatient(BluetoothDevice device) {
        Log.i(Constants.TAG_AC, CLASSNAME+" setDeviceToPatient");

        if (mBoundService.getSelectedPatient() != null) {
            // Récupération du TrackerManager pour enregistrer le nouveau device
            ActivityTrackerRepository manager = new ActivityTrackerRepository(this);

            // Création de l'activity tracker à partir du device
            activityTracker = new ActivityTracker(0, mBoundService.getSelectedPatient().getId(), device.getName(), device.getAddress());

            // On supprimer le tracker déjà existant
            manager.delete(manager.get(mBoundService.getSelectedPatient()));

            // Enregistrement du tracker
            activityTracker = manager.save(activityTracker);

            mBoundService.getSelectedPatient().setPodometre(activityTracker);

            patientDataController.updateTvActivityTracker(activityTracker);

            requestPatientInfos();
        }

    }

    /**
     *
     * @param userInfo
     */
    public void updatePatientInfo(BleUserInfo userInfo) {
        mBoundService.getActivityTrackerBLEService().saveUserInfos(userInfo);
    }

    private void requestPatientInfos() {
        mBoundService.getActivityTrackerBLEService().connectActivityTracker(ActivityTrackerBLEService.FLAG_BLE_ACTION_GET_INFOS);
    }

}
