package com.utbm.activity_tracker.ui.viewcontroller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.semantic.ecare_android_v2.R;
import com.semantic.ecare_android_v2.object.Patient;
import com.utbm.activity_tracker.core.model.ActivityTracker;
import com.utbm.activity_tracker.core.repository.ActivityTrackerRepository;
import com.utbm.activity_tracker.core.model.BleUserInfo;
import com.utbm.activity_tracker.ui.activities.PodometerSettingActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Julien on 02/05/16.
 */
public class PatientDataController {


    /**
     * Conteneur de l'alerte
     */
    private RelativeLayout alertViewRelativeLayout;

    /**
     * Texte de l'alerte
     */
    private TextView tvDateLastSync;
    private TextView tvPatientFootLength;
    private TextView tvPatientActivityTracker;
    private TextView tvPatientHeight;
    private TextView tvPatientAge;
    private TextView tvPatientWeight;
    private TextView tvPatientGender;


    private PodometerSettingActivity context;

    private SimpleDateFormat simpleDateFormat;

    private ActivityTrackerRepository activityTrackerRepository;

    private BleUserInfo patientInfo;

    /**
     * Constructeur
     */
    public PatientDataController(final PodometerSettingActivity context) {
        this.context                    = context;

        // Instantiation des vues
        this.tvDateLastSync             = (TextView) context.findViewById(R.id.tvPatientDateLastSyncTracker);
        this.tvPatientFootLength        = (TextView) context.findViewById(R.id.tvPatientFootLength);
        this.tvPatientActivityTracker   = (TextView) context.findViewById(R.id.tvPatientActivityTracker);
        this.tvPatientHeight            = (TextView) context.findViewById(R.id.tvPatientHeight);
        this.tvPatientWeight            = (TextView) context.findViewById(R.id.tvPatientWeight);
        this.tvPatientGender            = (TextView) context.findViewById(R.id.tvPatientGender);
        this.tvPatientAge               = (TextView) context.findViewById(R.id.tvPatientAge);

        this.simpleDateFormat           = new SimpleDateFormat("EEEE d MMM yyyy H:m", Locale.FRANCE);
        this.activityTrackerRepository  = new ActivityTrackerRepository(context);

        Button bnPatientFootLengthEdit = (Button) context.findViewById(R.id.bnPatientFootLengthEdit);
        bnPatientFootLengthEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Si les infos n'ont pas encore été récupérée on affiche un toast
                if(patientInfo != null) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final EditText edittext = new EditText(context);
                    edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                    alert.setMessage("Enter your foot length in centimeters");
                    alert.setTitle("Foot length");

                    alert.setView(edittext);

                    alert.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            patientInfo.setFootLength(Integer.valueOf(edittext.getText().toString()));
                            context.updatePatientInfo(patientInfo);

                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // what ever you want to do with No option.
                        }
                    });

                    alert.show();

                } else {
                    Toast.makeText(context, "La mise à jour des informations du patient nécéssite la récupération préalable des données existantes", Toast.LENGTH_SHORT).show();
                }


            }

        });

    }

    /**
     * Mise à jour du texte
     * @param date
     */
    public void setTvDateLastSync(Date date) {

        if( date != null ) {
            this.tvDateLastSync.setText(this.simpleDateFormat.format(date));
        } else {
            this.tvDateLastSync.setText("Jamais synchronisé");
        }
    }

    /**
     *
     * @param patient
     */
    public void updateTvDateLastSync(Patient patient) {
        this.setTvDateLastSync(activityTrackerRepository.getLastSyncDate(patient));
    }

    public void updateTvActivityTracker(ActivityTracker activityTracker) {
        if(activityTracker != null) {
            this.tvPatientActivityTracker.setText(String.format("%s - %s", activityTracker.getName(), activityTracker.getAddress()));
        } else {
            this.tvPatientActivityTracker.setText("Aucun podomètre associé");
        }
    }


    public void updateTvPatientFootLength(int length) {
        this.tvPatientFootLength.setText(length + " centimètres");
    }

    public void updateTvPatientGender(int gender) {
        this.tvPatientGender.setText(gender == 1 ? "Homme" : "Femme");
    }

    public void updateTvPatientAge(int age) {
        this.tvPatientAge.setText(age + " ans");
    }

    public void updateTvPatientHeight(int height) {
        this.tvPatientHeight.setText(height + " centimètres");
    }

    public void updateTvPatientWeight(int weight) {
        this.tvPatientWeight.setText(weight + " kg");
    }


    public void setPatientInfos(BleUserInfo patientInfo) {
        this.patientInfo = patientInfo;
        updateView();
    }

    public void updateView() {
        this.updateTvPatientFootLength(patientInfo.getFootLength());
        this.updateTvPatientAge(patientInfo.getAge());
        this.updateTvPatientGender(patientInfo.getGender());
        this.updateTvPatientHeight(patientInfo.getHeight());
        this.updateTvPatientWeight(patientInfo.getWeight());
    }

    public BleUserInfo getPatientInfo() {
        return patientInfo;
    }
}
