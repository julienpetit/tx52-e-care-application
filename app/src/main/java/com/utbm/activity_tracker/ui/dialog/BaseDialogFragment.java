package com.utbm.activity_tracker.ui.dialog;

import android.app.Activity;
import android.app.DialogFragment;

/**
 * Created by Julien on 09/05/16.
 */
public abstract class BaseDialogFragment<T> extends DialogFragment {
    private T mActivityInstance;

    public final T getActivityInstance() {
        return mActivityInstance;
    }

    @Override
    public void onAttach(Activity activity) {
        mActivityInstance = (T) activity;
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityInstance = null;
    }
}