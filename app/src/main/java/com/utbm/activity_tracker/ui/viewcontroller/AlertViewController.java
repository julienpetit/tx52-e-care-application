package com.utbm.activity_tracker.ui.viewcontroller;

import android.app.Activity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semantic.ecare_android_v2.R;

/**
 * Controleur du bandeau d'alerte qui s'affiche en haut des vues sur fond bleu, incluse dans header_ecare.xml
 *
 * Utilisable génériquement sur toute les vue pour une meilleure délégation des tâches
 *
 * Created by Julien on 26/04/16.
 */
public class AlertViewController {

    /**
     * Conteneur de l'alerte
     */
    private RelativeLayout alertViewRelativeLayout;

    /**
     * Texte de l'alerte
     */
    private TextView alertViewTextView;

//    private RelativeLayout loadingRelativeLayout;

    private Activity context;

    /**
     * Constructeur
     */
    public AlertViewController(Activity context, int alertViewRelativeLayoutId, int alertViewTextViewLayoutId) {
        this.context                    = context;
        this.alertViewRelativeLayout    = (RelativeLayout) context.findViewById(alertViewRelativeLayoutId);
        this.alertViewTextView          = (TextView) this.alertViewRelativeLayout.findViewById(alertViewTextViewLayoutId);
//        this.loadingRelativeLayout      = (RelativeLayout) this.alertViewRelativeLayout.findViewById(R.id.loadingNotice);
    }

    /**
     *
     * @param visibility
     */
    public void setAlertVisible(final boolean visibility) {
        if(alertViewRelativeLayout != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alertViewRelativeLayout.setVisibility(visibility ? RelativeLayout.VISIBLE : RelativeLayout.GONE);
                }
            });
        }
    }

    /**
     * Mise à jour du texte de l'alert
     * @param text
     */
    public void setText(final String text) {
        if(alertViewTextView != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alertViewTextView.setText(text);
                }
            });
        }
    }

//    /**
//     *
//     * @param loading
//     */
//    public void setLoading(final boolean loading) {
//        if(this.loadingRelativeLayout != null) {
//            context.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    loadingRelativeLayout.setVisibility(loading ? RelativeLayout.VISIBLE : RelativeLayout.GONE);
//                }
//            });
//        }
//    }


}
