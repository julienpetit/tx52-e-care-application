package com.utbm.activity_tracker.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.semantic.ecare_android_v2.R;
import com.semantic.ecare_android_v2.object.Patient;

import java.util.ArrayList;

/**
 * Created by Julien on 09/05/16.
 */
public class SetPatientActivityTrackerDialogFragment extends BaseDialogFragment<SetPatientActivityTrackerDialogFragment.OnDialogFragmentClickListener> {


    /**
     *
     */
    private final int DIALOG_EXIT = 1;

    /**
     *
     */
    private static final int REQUEST_ENABLE_BT = 1;

    /**
     * Temps maximum de scan
     */
    private static final long SCAN_PERIOD = 10000;

    /**
     * Nom de la classe
     */
    private String CLASSNAME = this.getClass().getName();

    /**
     * Adapter de la liste des objets bluetooth scannés
     */
    private LeDeviceListAdapter mLeDeviceListAdapter;

    private BluetoothAdapter mBluetoothAdapter;

    /**
     * Booléen permettant de savoir si un scan est en cours ou non
     */
    private boolean mScanning;

    private Patient patient;

    /**
     * Handler
     */
    private Handler mHandler;

    private AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id)
        {
            //onListItemClick((ListView)parent, v, position, id);
        }
    };

    private ListView devicesFoundListView;


    // interface to handle the dialog click back to the Activity
    public interface OnDialogFragmentClickListener {
        public void onOkClicked(SetPatientActivityTrackerDialogFragment dialog, Bundle bundle);
        public void onCancelClicked(SetPatientActivityTrackerDialogFragment dialog, Bundle bundle);
    }

    // Create an instance of the Dialog with the input
    public static SetPatientActivityTrackerDialogFragment newInstance(String title) {
        SetPatientActivityTrackerDialogFragment frag = new SetPatientActivityTrackerDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);


        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();



    }


    // Create a Dialog using default AlertDialog builder , if not inflate custom view in onCreateView
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mLeDeviceListAdapter = new LeDeviceListAdapter();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString("title"))
                .setCancelable(true)
                .setSingleChoiceItems(mLeDeviceListAdapter, 0, null)
                .setPositiveButton("Associer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                                BluetoothDevice device = mLeDeviceListAdapter.getDevice(selectedPosition);

                                Bundle bundle = new Bundle();
                                bundle.putParcelable("device", device);

                                // Positive button clicked
                                getActivityInstance().onOkClicked(SetPatientActivityTrackerDialogFragment.this, bundle);
                            }
                        }
                )
                .setNegativeButton("Annuler",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // negative button clicked
                                getActivityInstance().onCancelClicked(SetPatientActivityTrackerDialogFragment.this, null);
                            }
                        }
                );

//        View view = getActivity().getLayoutInflater().inflate(R.layout.activity_dialog_set_patient_at, null);
//        builder.setView(view);



        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();

//        this.devicesFoundListView = (ListView) getDialog().findViewById(R.id.lvPatientActivityTrackerFound);
//        this.devicesFoundListView.setOnItemClickListener(mOnClickListener);


        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();


        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Initializes list view adapter.
       //mLeDeviceListAdapter = new LeDeviceListAdapter();
        //this.devicesFoundListView.setAdapter(mLeDeviceListAdapter);
        scanLeDevice(true);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    //invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        //invalidateOptionsMenu();
    }

    /**
     * Classe privée, adapter de liste pour afficher les devices bluetooth
     */
    private class LeDeviceListAdapter extends BaseAdapter {

        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = SetPatientActivityTrackerDialogFragment.this.getActivity().getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View row;
            if (null == view) {
                row = mInflator.inflate(android.R.layout.select_dialog_singlechoice, null);

            } else {
                row = view;
            }

            TextView tv = (TextView) row.findViewById(android.R.id.text1);

            BluetoothDevice device = (BluetoothDevice) getItem(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                tv.setText(deviceName);
            else
                tv.setText(R.string.unknown_device);


            return row;

        }
    }
            /*ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listview_bracelet_item, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    /**
     * Callback appelé à chaque nouvelle détection de device bluetooth
     * On ajoute ce device à la liste affichée
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mLeDeviceListAdapter.addDevice(device);
//                            mLeDeviceListAdapter.notifyDataSetChanged();
//                        }
//                    });


                    mLeDeviceListAdapter.addDevice(device);
                    mLeDeviceListAdapter.notifyDataSetChanged();

                }
            };

    /**
     * View holder servant à enregistrer les informations de chaque éléments de la liste des devices
     */
    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreateView(inflater, container, savedInstanceState);



        View view = inflater.inflate(, container, false);
        return view;
    }
*/

}