package com.utbm.activity_tracker.core.utils;

import com.utbm.activity_tracker.core.model.BleUserInfo;

/**
 * Created by Julien on 19/04/16.
 */
public class ActivityTrackerCommand {


    public static byte[] getActivityAllDays(int day) {
        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x07;
        bytes[1] = (byte) (day & 0xFF);
        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }

    public static byte[] getActivityDay(int day) {
        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x43;
        bytes[1] = (byte) (day & 0xFF);
        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }


    public static byte[] startRealtimeMode() {
        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x09;
        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }


    public static byte[] getTime() {
        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x41;
        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }

    public static byte[] setCurrentTime() {

        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x01;

        // TODO
//        Date date = new Date();
//        date.


        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }

    /**
     *
     * @return
     */
    public static byte[] getUserPersonalInformation() {

        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x42;
        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }

    /**
     *
     * @param userInfo
     * @return
     */
    public static byte[] setUserPersonalInformation(BleUserInfo userInfo) {

        byte[] bytes = new byte[16];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) 0;
        }

        bytes[0] = (byte) 0x02;
        bytes[1] = (byte) (userInfo.getGender() & 0xFF);
        bytes[2] = (byte) (userInfo.getAge() & 0xFF);
        bytes[3] = (byte) (userInfo.getHeight() & 0xFF);
        bytes[4] = (byte) (userInfo.getWeight() & 0xFF);
        bytes[5] = (byte) (userInfo.getFootLength() & 0xFF);

        bytes[15] = (byte) checksum(bytes, 15);

        return bytes;
    }


    private static int checksum(byte[] bytes, int size) {

        int sum = 0;

        for (int i = 0; i < size; i++) {
            sum += bytes[i];
        }

        return sum;
    }

}
