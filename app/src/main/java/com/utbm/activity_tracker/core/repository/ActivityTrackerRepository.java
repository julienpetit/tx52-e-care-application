package com.utbm.activity_tracker.core.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.semantic.ecare_android_v2.object.Measure;
import com.semantic.ecare_android_v2.object.Patient;
import com.semantic.ecare_android_v2.util.Constants;
import com.semantic.ecare_android_v2.util.DataBaseConnector;
import com.utbm.activity_tracker.core.model.ActivityTracker;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Julien on 06/04/16.
 */
public class ActivityTrackerRepository {

    /**
     * Nom de la classe
     */
    private String CLASSNAME = this.getClass().getName();

    /**
     * Context
     */
    private Context context;

    private DataBaseConnector dbc;


    /**
     *
     * @param context
     */
    public ActivityTrackerRepository(Context context) {
        this.context    = context;
        this.dbc        = new DataBaseConnector(context);
    }

    /**
     * Récupère le tracker d'activité du patient
     * @param patient
     * @return
     */
    public ActivityTracker get(Patient patient) {

        ActivityTracker activityTracker = null;

        if (patient == null) {
            return null;
        }

        Log.i(Constants.TAG_AC, CLASSNAME + " recherche du podometre du patient " + patient.getUid());


        // Récupération du podomètre associé au patient
        SQLiteDatabase db = dbc.openRead();
        if(db!=null) {

            String query = String.format("SELECT capteur.id, type, adresse_mac FROM %s patient INNER JOIN %s capteur ON patient.id_podometre = capteur.id WHERE patient.id=?", Constants.TABLE_PATIENT, Constants.TABLE_CAPTEUR);

            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(patient.getId())});


            if (cursor != null) {

                Log.i(Constants.TAG_AC, CLASSNAME + " cursor : " + cursor.toString());

                if (cursor.moveToFirst()) {

                    activityTracker = new ActivityTracker(cursor.getInt(0), patient.getId(), cursor.getString(1), cursor.getString(2));

                }
                cursor.close();
            }

        }

        return activityTracker;
    }

    /**
     * Met à jour le tracker d'activité
     * @param activityTracker
     */
    public ActivityTracker save(ActivityTracker activityTracker) {

        delete(activityTracker);

        SQLiteDatabase db = dbc.openWrite();

        ContentValues cv = new ContentValues();
        cv.put("id_type", Measure.SENSOR_PAS);
        cv.put("adresse_mac", activityTracker.getAddress());
        cv.put("type", activityTracker.getName());

        long id = db.insert(Constants.TABLE_CAPTEUR, null, cv);
        if (id > 0) {
            activityTracker.setId(id);

            cv.clear();
            cv.put("id_podometre", id);

            if(db.update(Constants.TABLE_PATIENT, cv, "id=?", new String[]{String.valueOf(activityTracker.getPatientId())}) < 0) {
                activityTracker = null;
            }

        } else {
            activityTracker = null;
        }

        db.close();


        return activityTracker;
    }

    /**
     * Supprime tracker d'activité du patient
     * @return
     */
    public ActivityTracker delete(ActivityTracker activityTracker) {

        if(activityTracker != null) {

            SQLiteDatabase db = dbc.openWrite();

            ContentValues cv = new ContentValues();
            cv.put("id_podometre", "NULL");
            if(db.update(Constants.TABLE_PATIENT, cv, "id=?", new String[]{String.valueOf(activityTracker.getPatientId())}) > 0) {
                db.delete(Constants.TABLE_CAPTEUR, "id=?", new String[]{String.valueOf(activityTracker.getId())});
            }

            db.close();
        }

        return activityTracker;
    }

    public Date getLastSyncDate(Patient patient) {

        Date date = null;

        if(patient == null) {
            return null;
        }

        SQLiteDatabase db = dbc.openRead();
        if(db!=null) {

            long dateLastMeasure = 0;


            //récupération de la date de la dernière mesure
            Cursor cursorLast = db.query(
                    Constants.TABLE_MESURE,
                    new String[]{"date"},
                    String.format("id_patient=\"%s\" AND (sensor=\"%d\" OR sensor=\"%d\" OR sensor=\"%d\" OR sensor=\"%d\")", patient.getUid(), Measure.SENSOR_CALORIE, Measure.SENSOR_DISTANCE, Measure.SENSOR_PAS, Measure.SENSOR_ACTIVITE_SOMMEIL),
                    null,
                    null,
                    null,
                    "date DESC",
                    "1"
            );

            if (cursorLast != null) {
                if (cursorLast.moveToFirst()) {
                    dateLastMeasure = cursorLast.getLong(0);
                }
                cursorLast.close();
            }

            if(dateLastMeasure != 0) {

                date = new Date();
                date.setTime(dateLastMeasure);

            }

        }

        return date;
    }

    /**
     *
     * @param patient
     * @param maxDaysToFetch
     * @return
     */
    public int getLastSyncDayCount(Patient patient, int maxDaysToFetch) {

        Date date = getLastSyncDate(patient);

        if(date != null) {
            Calendar fromCalender = Calendar.getInstance();
            fromCalender.setTime(date);
            Calendar toCalender = Calendar.getInstance();
            toCalender.setTime(new Date());

            long diffmili = toCalender.getTimeInMillis() - fromCalender.getTimeInMillis();

            maxDaysToFetch = Long.valueOf(TimeUnit.MILLISECONDS.toDays(diffmili)).intValue();
        }

        Log.d(CLASSNAME, "DIFFERENCE DAYS : " + maxDaysToFetch);

        return maxDaysToFetch;
    }


}
