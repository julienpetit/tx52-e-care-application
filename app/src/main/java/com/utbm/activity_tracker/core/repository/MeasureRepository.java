package com.utbm.activity_tracker.core.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.semantic.ecare_android_v2.object.Measure;
import com.semantic.ecare_android_v2.object.Patient;
import com.semantic.ecare_android_v2.util.Constants;
import com.semantic.ecare_android_v2.util.DataBaseConnector;
import com.utbm.activity_tracker.core.model.ActivityTracker;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Julien on 06/04/16.
 */
public class MeasureRepository {

    /**
     * Nom de la classe
     */
    private String CLASSNAME = this.getClass().getName();

    /**
     * Context
     */
    private Context context;

    private DataBaseConnector dbc;


    /**
     *
     * @param context
     */
    public MeasureRepository(Context context) {
        this.context    = context;
        this.dbc        = new DataBaseConnector(context);
    }

    public Integer get(Patient patient, Date date, int sensor) {

        if (patient == null || date == null) {
            return null;
        }

        Log.i(Constants.TAG_AC, CLASSNAME + " recherche du podometre du patient " + patient.getUid());

        Integer measureId = null;

        // Récupération du podomètre associé au patient
        SQLiteDatabase db = dbc.openRead();
        if(db!=null) {

            String query = String.format("SELECT id FROM %s measure WHERE measure.date=?", Constants.TABLE_MESURE);

            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(date.getTime())});

            if (cursor != null) {

                if (cursor.moveToFirst()) {

                    measureId = cursor.getInt(0);

                }
                cursor.close();
            }

        }

        return measureId;
    }

    public void update(Measure measure) {

    }

    /**
     *
     * @param measure
     * @param patient
     * @return
     */
    public long createOrUpdateMeasure(Measure measure, Patient patient) {

        long res =-1;

        Integer measureId = get(patient, measure.getDate(), measure.getSensor());

        ContentValues cv = new ContentValues();
        cv.put("id_patient", patient.getUid());
        cv.put("sensor", measure.getSensor());
        cv.put("date", measure.getDate().getTime());
        cv.put("valeur", measure.getValue());
        cv.put("note", measure.getNote());
        cv.put("noteDate", measure.getNoteDate());
        cv.put("synchronized", 0);

        DataBaseConnector dbc = new DataBaseConnector(context);
        SQLiteDatabase db = dbc.openWrite();

        if(measureId == null) {

            // CREATE
            if(db!=null){
                res = db.insert(Constants.TABLE_MESURE,null,cv);
                db.close();
            }

        } else {
            // UPDATE
            if(db!=null){
                res = db.update(Constants.TABLE_MESURE, cv, "id = ?", new String[]{String.valueOf(measureId)});
                db.close();
            }
        }

        dbc.close();

        return res;

    }


}
