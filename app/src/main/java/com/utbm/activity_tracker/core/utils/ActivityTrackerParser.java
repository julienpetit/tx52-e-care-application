package com.utbm.activity_tracker.core.utils;

import android.util.Log;

import com.semantic.ecare_android_v2.object.CompoundMeasure;
import com.semantic.ecare_android_v2.object.Measure;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Julien on 25/04/16.
 */
public class ActivityTrackerParser {

    private final static String TAG = ActivityTrackerParser.class.getSimpleName();

    public static CompoundMeasure parseNightActivity(byte[] value) {

        CompoundMeasure cm = new CompoundMeasure();

        Date date = parseDateFromBytes(value);

        for(int i = 7; i <= 14; i++ ) {

            if (date != null) {
                Date currentDate = (Date) date.clone();

                currentDate = addMinutesToDate((i-7) * 2, currentDate);

                int minuteValue = value[i];

                if (minuteValue > 0) {
                    cm.add(new Measure(Measure.SENSOR_ACTIVITE_SOMMEIL, minuteValue, currentDate, true));
                }

            }

        }

        return cm;

    }

    public static CompoundMeasure parseDayActivity(byte[] value) {

        CompoundMeasure cm = new CompoundMeasure();

        // The calorie be divided by 100 and reserve two decimal
        int calories        = ActivityTrackerParser.mergeBytes(value[7], value[8], null) / 100;
        int steps           = ActivityTrackerParser.mergeBytes(value[9], value[10], null);
        int distance        = ActivityTrackerParser.mergeBytes(value[11], value[12], null);
        int runningSteps    = ActivityTrackerParser.mergeBytes(value[13], value[14], null);

        // On vérifie que la trame reçu contient des informations d'activité
        if( calories == 0 && steps == 0 && distance == 0 && runningSteps == 0) {
            return cm;
        }

        // Number in minute in the day
        int timeScale = value[5] * 15;
        int hours = timeScale / 60;
        int minutes = timeScale % 60;

        String strDate = convertDToH(value[2]) + "-" + convertDToH(value[3]) + "-" + convertDToH(value[4]) + " " + hours + ":" + minutes;

        // On parse la date et on tente l'enregistrement de la mesure

        Date date = parseDateFromBytes(value);

        if(date != null) {

            if (calories > 0) {
                cm.add(new Measure(Measure.SENSOR_CALORIE, calories, date, true));
            }

            if (distance > 0) {
                cm.add(new Measure(Measure.SENSOR_DISTANCE, distance, date, true));
            }

            if (steps > 0) {
                cm.add(new Measure(Measure.SENSOR_PAS, steps, date, true));
            }

        }

        return cm;

    }

    /**
     *
     * @param value
     * @return
     */
    public static CompoundMeasure parse(byte[] value) {

        if(value[6] == (byte) 0x00) {
            return parseDayActivity(value);
        } else {
            return parseNightActivity(value);
        }

    }

    /**
     *
     * @param value
     * @return
     */
    public static Date parseDateFromBytes(byte[] value) {

        if(value[2] == 0) {
            return null;
        }

        Date date = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yy-M-d H:m", Locale.FRANCE); // Set your date format

        // Number in minute in the day
        int timeScale = value[5] * 15;
        int hours = timeScale / 60;
        int minutes = timeScale % 60;

        String strDate = convertDToH(value[2]) + "-" + convertDToH(value[3]) + "-" + convertDToH(value[4]) + " " + hours + ":" + minutes;

        try {
            date = sdf.parse(strDate);
        }
        catch (ParseException e) {
            Log.e(TAG, "Unable to parse date " + strDate );
        }

        return date;
    }

    public static String convertDToH(int n) {
        return Integer.toHexString(n);
    }


    /**
     * Convert all mesure to split per day
     * @param compoundMeasure
     * @return
     */
    public static CompoundMeasure normalizeMeasures(CompoundMeasure compoundMeasure){

        HashMap<Date, Measure> hmMeasureDistance        = new HashMap<>();
        HashMap<Date, Measure> hmMeasureCalorie         = new HashMap<>();
        HashMap<Date, Measure> hmMeasurePas             = new HashMap<>();

        HashMap<Integer, HashMap<Date, Measure>> aMap = new HashMap<>();

        aMap.put(Measure.SENSOR_DISTANCE, hmMeasureDistance);
        aMap.put(Measure.SENSOR_CALORIE, hmMeasureCalorie);
        aMap.put(Measure.SENSOR_PAS, hmMeasurePas);

        // Nouvelle mesures normalisées
        CompoundMeasure newCompoundMeasure = new CompoundMeasure();

        Iterator<Measure> it = compoundMeasure.iterator();
        while(it.hasNext()) {

            Measure measure = it.next();

            // Si c'est une mesure d'activité de sommeil, on ne doit pas modifer la date, on l'ajouter telle qu'elle.
            if(measure.getSensor() == Measure.SENSOR_ACTIVITE_SOMMEIL) {
                newCompoundMeasure.add(measure);
                continue;
            }

            // Le cas ci-dessous gère le reste des mesure, pour créer des mesures avec des valeurs par jour.
            Date date = ActivityTrackerParser.removeTimeFromDate(measure.getDate());

            if(date == null) {
                Log.e(TAG, "normalizeMeasures date null. Skipping to next measure");
                break;
            }

            HashMap<Date, Measure> currentHm = aMap.get(measure.getSensor());

            if(currentHm == null) {
                Log.e(TAG, "normalizeMeasures currentHm null. Skipping to next measure");
                break;
            }

            if(currentHm.get(date) == null) {
                Measure existingMeasure = new Measure(measure.getSensor(), measure.getValue(), date, false, measure.getMeasureId());
                currentHm.put(date, existingMeasure);
            } else {

                Measure existingMeasure = currentHm.get(date);
                existingMeasure.setValue(existingMeasure.getValue() + measure.getValue());
            }

        }

        Iterator it2 = aMap.entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry pair = (Map.Entry)it2.next();

            Iterator it3 = ((HashMap<Date, Measure>) pair.getValue()).entrySet().iterator();
            while (it3.hasNext()) {

                Map.Entry pair2 = (Map.Entry)it3.next();

                newCompoundMeasure.add((Measure) pair2.getValue());

                it3.remove();
            }

            it2.remove(); // avoids a ConcurrentModificationException
        }
        
        return newCompoundMeasure;
    }

    /**
     * Remove time from a date
     * @param date
     * @return
     */
    private static Date removeTimeFromDate(Date date) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String s = formatter.format(date);

        try {
            date = formatter.parse(s);

        } catch (ParseException e) {
            date = null;
        }

        return date;
    }


    /**
     *
     * @param value
     * @return
     */
    public static ArrayList<Measure> parseRealTimeData(byte[] value) {

        ArrayList<Measure> listMeasures = new ArrayList<>();

        if(value[0] == (byte) 0x09) {
            int calories        = mergeBytes(value[7], value[8], value[9]) / 100;
            int steps           = mergeBytes(value[1], value[2], value[3]);
            int distance        = mergeBytes(value[10], value[11], value[12]);
            int runningSteps    = mergeBytes(value[4], value[5], value[6]);

            Date date = removeTimeFromDate(new Date());

            Measure mCalories = new Measure(Measure.SENSOR_CALORIE, calories, date, false);
            Measure mSteps = new Measure(Measure.SENSOR_PAS, steps, date, false);
            Measure mDistance = new Measure(Measure.SENSOR_DISTANCE, distance, date, false);

            listMeasures.add(mCalories);
            listMeasures.add(mSteps);
            listMeasures.add(mDistance);
        }

        return listMeasures;
    }

    private static int mergeBytes(Byte b1, Byte b2, Byte b3) {


       if(b3 != null) {
            return 256 * 256 * b1 + 256 * b2 + b3;
       } else {
           return 256 * b1 + b2;
       }


    }

    private static Date addMinutesToDate(int minutes, Date beforeTime){
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

        long curTimeInMs = beforeTime.getTime();
        return new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
    }
}
