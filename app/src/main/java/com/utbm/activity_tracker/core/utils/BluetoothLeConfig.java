package com.utbm.activity_tracker.core.utils;

import java.util.UUID;

/**
 * Created by Julien on 12/05/16.
 */
public class BluetoothLeConfig {

    public final static UUID    UUID_SERVICE_ACTIVITY_TRACKER  = UUID.fromString("0000FFF0-0000-1000-8000-00805f9b34fb");
    public final static UUID    UUID_CHARACTERISTICUUID_TX     = UUID.fromString("0000FFF6-0000-1000-8000-00805f9b34fb");
    public final static UUID    UUID_CHARACTERISTICUUID_RX     = UUID.fromString("0000FFF7-0000-1000-8000-00805f9b34fb");
    public final static UUID    UUID_CLIENT_CONFIG_DESCRIPTOR  = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

}
