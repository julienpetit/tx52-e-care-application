package com.utbm.activity_tracker.core.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import android.os.IBinder;
import android.util.Log;

import com.semantic.ecare_android_v2.core.ServiceEcare;
import com.semantic.ecare_android_v2.object.CompoundMeasure;
import com.semantic.ecare_android_v2.object.Measure;
import com.semantic.ecare_android_v2.util.Constants;
import com.utbm.activity_tracker.core.repository.MeasureRepository;
import com.utbm.activity_tracker.core.utils.ActivityTrackerCommand;
import com.utbm.activity_tracker.core.utils.ActivityTrackerParser;
import com.utbm.activity_tracker.core.repository.ActivityTrackerRepository;
import com.utbm.activity_tracker.core.model.BleUserInfo;
import com.utbm.activity_tracker.core.utils.BluetoothLeConfig;
import com.utbm.activity_tracker.core.model.ActivityTracker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Julien on 19/04/16.
 */
public class ActivityTrackerBLEService {

    private final static String TAG = ActivityTrackerBLEService.class.getSimpleName();

    private ServiceEcare serviceEcare;


    /**
     *
     */
    public final static String FLAG_BLE_ACTION_GET_MEASUREMENTS = "get_mesurements";
    public final static String FLAG_BLE_ACTION_GET_INFOS        = "get_infos";

    /**
     * récupération des nouvelles données toutes les 30 minutes
     */
    public final static long INTERVAL_GET_MESUREMENTS               = 1000 * 60 * 30;

    public final static long NB_REAL_TIME_MEASURES_BEFORE_SAVING    = 15;

    private int realTimeMeasureCount = 0;

    private String bleCurrentActionFlag = null;

    private CompoundMeasure podometerMesures;

    private Timer timer = new Timer();

    public ActivityTrackerBLEService(ServiceEcare serviceEcare) {
        this.serviceEcare = serviceEcare;
    }

    /**
     *
     */
    public void startActivityTrackerService() {

        if(mBluetoothLeService == null) {

            // Inscription aux evènements BLE
            serviceEcare.registerReceiver(mGattUpdateReceiver, BluetoothLeService.makeGattUpdateIntentFilter());

            // Lancement du service BLE
            Intent gattServiceIntent = new Intent(serviceEcare, BluetoothLeService.class);
            serviceEcare.bindService(gattServiceIntent, mServiceConnection, Service.BIND_AUTO_CREATE);

            net.newel.android.Log.i(Constants.TAG, TAG + " lancement du service");

        }

    }

    /**
     * Service activity tracker
     */
    private BluetoothLeService mBluetoothLeService;

    private int daysToFetch = 0;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {

            net.newel.android.Log.i(Constants.TAG_AC, "Bluetooth service connected to eCareService");

            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service)
                    .getService();
            if (!mBluetoothLeService.initialize()) {
                net.newel.android.Log.e(Constants.TAG_AC, "Unable to initialize Bluetooth");
            }


            startTaskMeasurements();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            net.newel.android.Log.i(Constants.TAG_AC, "Bluetooth service diconnected to eCareService");

            if (mBluetoothLeService != null) {
                mBluetoothLeService.disconnect();
                mBluetoothLeService.close();
            }

            mBluetoothLeService = null;
        }
    };

    /**
     *
     */
    public void updatePatientActivityTracker() {

        // Mise à jour du podomètre du patient
        ActivityTrackerRepository manager = new ActivityTrackerRepository(this.serviceEcare.getApplicationContext());

        // Création de l'activity tracker à partir du device
        this.serviceEcare.getSelectedPatient().setPodometre(manager.get(serviceEcare.getSelectedPatient()));

    }

    /**
     *
     */
    public void connectActivityTracker(String flag) {

        ActivityTracker device = serviceEcare.getSelectedPatient().getPodometre();

        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnect();

            // Sleep to wait for disconnect (maybe fixes Gatt server errors and
            // reconnection issue)
            // Gatt server error, status: 133, newState: 2
            // Gatt server error, status: 257, newState: 0
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            mBluetoothLeService.close();

            if(device != null) {

                // Log functional
                if(flag.equals(FLAG_BLE_ACTION_GET_MEASUREMENTS)) {

                    // Mise à jour du nombre de jours à synchroniser
                    updateDayToFetch();

                    // Initialisation des mesures
                    this.podometerMesures = new CompoundMeasure();

                    net.newel.android.Log.i(Constants.TAG_AC, "EcareService : startMeasurements");
                } else if(flag.equals(FLAG_BLE_ACTION_GET_INFOS)) {
                    net.newel.android.Log.i(Constants.TAG_AC, "EcareService : startGetInfos");
                }

                bleCurrentActionFlag = flag;
                if(mBluetoothLeService.isConnected() && mBluetoothLeService.getSupportedGattServices() != null) {

                    if(flag.equals(FLAG_BLE_ACTION_GET_MEASUREMENTS)) {
                        if(daysToFetch == -1) {
                            requestRealTimeMode();
                        } else {
                            requestTrackerActivity();
                        }
                    } else if(flag.equals(FLAG_BLE_ACTION_GET_INFOS)) {
                        requestUserInfos();
                    }

                } else {
                    mBluetoothLeService.connect(device.getAddress());
                }
                // reconnectDelayed();
            } else {
                net.newel.android.Log.w(Constants.TAG_AC, "connectActivityTracker : not podometer associated with this patient");
            }
        } else {
            net.newel.android.Log.e(Constants.TAG_AC, "connectActivityTracker : mBluetoothLeService null");
        }

    }

    /**
     *
     */
    public void requestTrackerActivity() {

        net.newel.android.Log.i(Constants.TAG_AC, "EcareService.requestTrackerActivity : dayToFetch " + daysToFetch);

        if(daysToFetch >= 0) {
            mBluetoothLeService.writeCharacteristic(
                    "RECUPERATION ACTIVITY DAY " + daysToFetch,
                    BluetoothLeConfig.UUID_SERVICE_ACTIVITY_TRACKER,
                    BluetoothLeConfig.UUID_CHARACTERISTICUUID_TX,
                    ActivityTrackerCommand.getActivityDay(daysToFetch--)
            );
        }

    }

    /**
     * Request real time mode to fetch date of the current day
     */
    public void requestRealTimeMode() {
        mBluetoothLeService.writeCharacteristic(
                "Start real time mode",
                BluetoothLeConfig.UUID_SERVICE_ACTIVITY_TRACKER,
                BluetoothLeConfig.UUID_CHARACTERISTICUUID_TX,
                ActivityTrackerCommand.startRealtimeMode()
        );
    }

    /**
     * Write data to the podometre to activate notifications et data receipt
     */
    public void requestActiveNotifications() {

        // Method will create and object to decribe what to send to the device
        mBluetoothLeService.setCharacteristicNotification(
                "NOTIFICATION DES DONNEES",
                BluetoothLeConfig.UUID_SERVICE_ACTIVITY_TRACKER,
                BluetoothLeConfig.UUID_CHARACTERISTICUUID_RX,
                true
        );

    }

    /**
     * Write data to the podometre to get the user infos
     */
    private void requestUserInfos() {

        mBluetoothLeService.writeCharacteristic(
                "GET PERSONAL INFO",
                BluetoothLeConfig.UUID_SERVICE_ACTIVITY_TRACKER,
                BluetoothLeConfig.UUID_CHARACTERISTICUUID_TX,
                ActivityTrackerCommand.getUserPersonalInformation()
        );
    }

    /**
     * Send data to the device to update user info like weight, height, footlength...
     * @param userInfo
     */
    public void saveUserInfos(BleUserInfo userInfo) {

        if(userInfo != null && userInfo.isValid()) {

            mBluetoothLeService.writeCharacteristic(
                    "GET PERSONAL INFO",
                    BluetoothLeConfig.UUID_SERVICE_ACTIVITY_TRACKER,
                    BluetoothLeConfig.UUID_CHARACTERISTICUUID_TX,
                    ActivityTrackerCommand.setUserPersonalInformation(userInfo)
            );

        } else {
            net.newel.android.Log.d(TAG, "user info not valid");

        }

    }

    /**
     * Receive notifications from BLE service : podomètre
     */
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                net.newel.android.Log.i(Constants.TAG_AC, "EcareService : tracker connected");



            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {

                net.newel.android.Log.i(Constants.TAG_AC, "EcareService : tracker disconnected");

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

                byte[] value = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);

                if(FLAG_BLE_ACTION_GET_MEASUREMENTS.equals(bleCurrentActionFlag)) {

                    // Saving measures
                    if( ActivityTrackerParser.convertDToH(value[0]).equals("43") && ActivityTrackerParser.convertDToH(value[1]).equals("fffffff0") ) {
                        CompoundMeasure newMesures = ActivityTrackerParser.parse(value);

                        Iterator<Measure> it = newMesures.iterator();
                        while(it.hasNext()) {
                            podometerMesures.add(it.next());
                        }

                    }

                    if(value[0] == (byte) 0x09) {

                        if(realTimeMeasureCount < NB_REAL_TIME_MEASURES_BEFORE_SAVING) {
                            realTimeMeasureCount++;
                        } else {
                            saveRealTimeMeasure(value);
                        }


                    }


                    if( (value[0] == (byte) 0x43 && value[1] == (byte) 0xF0 && value[5] == 95)
                            || value[0] == (byte) 0x43 && value[1] == -1) {

                        if(daysToFetch < 0) {
                            if(podometerMesures != null && podometerMesures.size() > 0) {

                                serviceEcare.newSensorMeasure(ActivityTrackerParser.normalizeMeasures(podometerMesures));
                                podometerMesures = new CompoundMeasure();
                            }

                            // @TODO
							requestRealTimeMode();

                            net.newel.android.Log.i(Constants.TAG_AC, "EcareService : END OF DAY SYNC");


                        } else {
                            requestTrackerActivity();
                        }
                    }
                }

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                net.newel.android.Log.i(Constants.TAG_AC, "EcareService : tracker services discovered");

                if(FLAG_BLE_ACTION_GET_MEASUREMENTS.equals(bleCurrentActionFlag)) {
                    requestActiveNotifications();

                    if(daysToFetch == -1) {
                        requestRealTimeMode();
                    } else {
                        requestTrackerActivity();
                    }
                } else if(FLAG_BLE_ACTION_GET_INFOS.equals(bleCurrentActionFlag)) {
                    requestActiveNotifications();
                    requestUserInfos();
                }

//				try {
//					Thread.sleep(2000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}

            }
        }
    };

    /**
     *
     */
    public void disconnectActivityTracker() {
        net.newel.android.Log.i(Constants.TAG_AC, "EcareService : disconnectActivityTracker");

        timer.cancel();
        timer = new Timer();

        bleCurrentActionFlag = null;
        mBluetoothLeService.disconnect();
        mBluetoothLeService.close();
    }

    /**
     *
     */
    private void updateDayToFetch() {
        ActivityTrackerRepository manager = new ActivityTrackerRepository(serviceEcare.getApplicationContext());

        // - 1 is to not fetch the data of the current day because it's not complete anymore
        daysToFetch = manager.getLastSyncDayCount(serviceEcare.getSelectedPatient(), 30) - 1;
    }

    /**
     * Schedule a task to get new measurements
     */
    public void startTaskMeasurements() {
        timer.cancel();
        timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                connectActivityTracker(FLAG_BLE_ACTION_GET_MEASUREMENTS);
            }
        };
        timer.schedule(task, 0, INTERVAL_GET_MESUREMENTS);
    }

    /**
     * Updating the real time measure in the database
     * @param value
     */
    private void saveRealTimeMeasure(byte[] value) {

        Log.i(Constants.TAG_AC, "EcareService : saveRealTimeMeasure counter : " + realTimeMeasureCount);

        MeasureRepository repository = new MeasureRepository(this.serviceEcare);

        ArrayList<Measure> listMeasures = ActivityTrackerParser.parseRealTimeData(value);

        for(Measure measure : listMeasures) {
            repository.createOrUpdateMeasure(measure, serviceEcare.getSelectedPatient());
        }

//        CompoundMeasure newMesures = ActivityTrackerParser.parseRealTime(value);

        // Re-initialisation of the counter
        realTimeMeasureCount = 0;
    }

}
