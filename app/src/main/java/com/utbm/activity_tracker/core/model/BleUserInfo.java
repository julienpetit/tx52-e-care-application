package com.utbm.activity_tracker.core.model;

/**
 * Created by Julien on 19/05/16.
 */
public class BleUserInfo {

    private Integer gender;
    private Integer age;
    private Integer height;
    private Integer weight;
    private Integer footLength;

    public BleUserInfo(int gender, int age, int height, int weight, int footLength) {
        this.gender = gender;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.footLength = footLength;
    }

    public BleUserInfo(byte[] value) {

        this.gender = (int) value[1];
        this.age = (int) value[2];
        this.height = (int) value[3];
        this.weight =  (int)value[4];
        this.footLength = (int) value[5];

    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getFootLength() {
        return footLength;
    }

    public void setFootLength(int footLength) {
        this.footLength = footLength;
    }

    public boolean isValid() {
        return (this.age != null && this.gender != null && this.height != null && this.weight!= null && this.footLength != null);
     }
}
