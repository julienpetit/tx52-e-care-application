package com.utbm.activity_tracker.core.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.util.Log;

import com.semantic.ecare_android_v2.util.Constants;
import com.utbm.activity_tracker.core.model.BleAction;

/**
 * Required Queue to serialize GATT commands
 *
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BleQueue {

    private Queue<BleAction> bleQueue = new LinkedList<BleAction>();
    private HashMap<UUID, BluetoothGattService> serviceCache = new HashMap<>();
    private BluetoothGatt mBluetoothGatt;

    public BleQueue(BluetoothGatt bluetoothGatt) {
        this.mBluetoothGatt = bluetoothGatt;
    }


    public void onDescriptorWrite(BluetoothGatt gatt,
                                  BluetoothGattDescriptor descriptor, int status) {
        Log.d(Constants.TAG_AC, "onDescriptorWrite ");

        bleQueue.poll();
        nextAction();
    };

    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic, int status) {
        Log.d(Constants.TAG_AC, "onCharacteristicRead " + java.util.Arrays.toString(characteristic.getValue()));
        bleQueue.poll();
        nextAction();
    }

    public void onCharacteristicChanged(BluetoothGattCharacteristic characteristic) {

        Log.d(Constants.TAG_AC, "onCharacteristicChanged " + java.util.Arrays.toString(characteristic.getValue()));

        byte[] value = characteristic.getValue();

        if( (value[0] == (byte) 0x43 && value[1] == (byte) 0xF0 && value[5] == 95)
                || value[0] == (byte) 0x43 && value[1] == -1) {
            Log.d(Constants.TAG_AC, "onCharacteristicChanged 95 FIN ");
        }

        // Si on a récupéré toutes les données d'un jour ou si pas de données sur le jour on récupère le jour suivant
//        if( (value[0] == (byte) 0x43 && value[1] == (byte) 0xF0 && value[5] == 95)
//                || value[0] == (byte) 0x43 && value[1] == -1) {
//            nextAction();
//        }
//
//        if( value[0] != (byte) 0x43 && value[0] != (byte) 0x07 ) {
//            nextAction();
//        }
    }


    public void onCharacteristicWrite(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic, int status) {
        Log.i(Constants.TAG_AC, " onCharacteristicWrite :: " + characteristic.getValue()[0] + " " + characteristic.getValue()[1]);

//        if(!characteristic.getUuid().equals(BluetoothLeConfig.UUID_CHARACTERISTICUUID_TX) && ) {
//        if( characteristic.getValue()[0] != (byte) 0x43 && characteristic.getValue()[0] != (byte) 0x07) {
//        //    bleQueue.remove();
//            nextAction();
//        }
        bleQueue.poll();
        nextAction();
    }

    public void addAction(BleAction action) {

        if(action.getActionType() == BleAction.ActionType.writeCharacteristic ) {
            Log.i(Constants.TAG_AC, " addAction :: " + action.getValues()[1]);
        }
        Log.i(Constants.TAG_AC, " addAction :ref: " + action.toString());

        bleQueue.add(action);

        Log.d(Constants.TAG_AC, "addAction : queue :  " + java.util.Arrays.toString(bleQueue.toArray()));

//        if(action.getActionType() == BleAction.ActionType.writeCharacteristic ) {
//            byte[] value = action.getValues();
//
//            if (value[0] == (byte) 0x43 || value[0] == (byte) 0x07 || value[0] == (byte) 0x41 || value[0] == (byte) 0x42) {
//                Log.i(Constants.TAG_AC, " NEXT ACTION SKIPPED ");
//
//                return;
//            }
//        }

        // if there is only 1 item in the queue, then process it. If more than
        // 1,
        // we handle asynchronously in the callback.
        if (bleQueue.size() == 1)
            nextAction();
    }

    public void nextAction() {

        if (bleQueue.isEmpty())
            return;


        BleAction action = bleQueue.peek();

        Log.i("BLEQueue", "nextAction() : " + action.toString());


        BluetoothGattService service = findService(action.getServiceUUID());
        if(service == null) {
            Log.e("BLEQueue", "nextAction() : Service not found " + action.getServiceUUID() + " " + action.getName());
            return;
        }

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(action.getObjectUUID());
        if(characteristic == null) {
            Log.e("BLEQueue", "nextAction() : Characteristic not found " + action.getObjectUUID());
            return;
        }

        if(action.getValues() == BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE) {
            mBluetoothGatt.setCharacteristicNotification(characteristic, true);
        }

        if (BleAction.ActionType.writeDescriptor.equals(action.getActionType())) {

            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(BluetoothLeConfig.UUID_CLIENT_CONFIG_DESCRIPTOR);
            if (descriptor != null) {
                descriptor.setValue(action.getValues());
                mBluetoothGatt.writeDescriptor(descriptor);
                Log.d(Constants.TAG_AC, "Write descriptor " + action.getName());
            }

        } else if (BleAction.ActionType.writeCharacteristic.equals(action.getActionType())) {

            characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            characteristic.setValue(action.getValues());
            mBluetoothGatt.writeCharacteristic(characteristic);

            Log.d(Constants.TAG_AC, "writeCharacteristic " + characteristic.toString());

        } else if (BleAction.ActionType.readCharacteristic.equals(action.getActionType())) {

            characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            characteristic.setValue(action.getValues());
            mBluetoothGatt.readCharacteristic(characteristic);

            Log.d(Constants.TAG_AC, "readCharacteristic " + characteristic.toString());


        } else {
            Log.e("BLEQueue", "Undefined Action found");
        }



    }

    /**
     *
     * @param serviceUUID
     * @return
     */
    private BluetoothGattService findService(UUID serviceUUID) {


        final List<BluetoothGattService> gattServices = mBluetoothGatt.getServices();

        // activityTrackerService Service
        return BleUtils.findService(serviceUUID, gattServices);



//        BluetoothGattService service;
//
//        if(serviceCache.get(serviceUUID) != null) {
//
//            Log.d(Constants.TAG_AC, "findService cache :  " + serviceCache.get(serviceUUID));
//
//            service = serviceCache.get(serviceUUID);
//        } else {
//
//            final List<BluetoothGattService> gattServices = mBluetoothGatt.getServices();
//
//            // activityTrackerService Service
//            service = BleUtils.findService(serviceUUID, gattServices);
//
//            if (service != null) {
//                serviceCache.put(serviceUUID, service);
//            }
//        }
//
//        return service;
    }


}
