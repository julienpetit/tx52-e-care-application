package com.utbm.activity_tracker.core.model;

/**
 * Created by Julien on 06/04/16.
 */
public class ActivityTracker {

    private long id;
    private int patientId;
    private String name;
    private String address;

    public ActivityTracker(int id, int patientId, String name, String address) {
        this.id         = id;
        this.patientId  = patientId;
        this.name       = name;
        this.address    = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
