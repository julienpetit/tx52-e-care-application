package com.utbm.activity_tracker.core.model;

import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Julien on 14/05/16.
 */
public class BleAction {

    public enum ActionType {
        writeDescriptor,
        readCharacteristic,
        writeCharacteristic
    }

    private ActionType actionType;
    private String name;
    private UUID serviceUUID;
    private UUID objectUUID;
    private byte[] values;

    public BleAction(ActionType actionType, String name, UUID serviceUUID, UUID objectUUID, byte[] values) {
        this.actionType = actionType;
        this.name = name;
        this.serviceUUID = serviceUUID;
        this.objectUUID = objectUUID;
        this.values = values;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getServiceUUID() {
        return serviceUUID;
    }

    public void setServiceUUID(UUID serviceUUID) {
        this.serviceUUID = serviceUUID;
    }

    public UUID getObjectUUID() {
        return objectUUID;
    }

    public void setObjectUUID(UUID objectUUID) {
        this.objectUUID = objectUUID;
    }

    public byte[] getValues() {
        return values;
    }

    public void setValues(byte[] values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "BleAction{" +
                "actionType=" + actionType +
                ", name='" + name + '\'' +
                ", serviceUUID=" + serviceUUID +
                ", objectUUID=" + objectUUID +
                ", values=" + Arrays.toString(values) +
                '}';
    }
}
