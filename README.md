

# Introduction

# Analyse de l'existant





# Mise en place
---------

Analyse du projet existant, mise à jour de l’environnement pour prendre en charge le nouveau gestionnaire dépendance d’un projet Android, Gradle.

### Mise à jour des permissions

Mise à jour du Manifest pour ajouter les permissions bluetooth à l’application.

``` xml
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
```

On déclare que l’application utilise du bluetooth energy, l’instruction permet de la spécifier dans le descriptif de l’application sur Google Play. On notera que la déclaration est non requise, ce qui aux utilisateur qui ne possèdent pas de puces Bluetooth Low Energy à installer l’application. Cependant, un message préviendra l’utilisateur qu’il ne poura pas utiliser la fonctionnalité de synchronisation des données du podometre. 

``` xml
<uses-feature android:name="android.hardware.bluetooth_le" android:required="false"/>
```

## Mise à jour les contantes
#### Unités
Ajout des unités pour les calories et la distance

``` java
...
SENSOR_UNIT.append(Measure.SENSOR_TEMPERATURE, R.string.sensor_unit_temperature);
SENSOR_UNIT.append(Measure.SENSOR_DISTANCE, R.string.sensor_unit_distance);
SENSOR_UNIT.append(Measure.SENSOR_CALORIE, R.string.sensor_unit_calorie);
```



## TODO

- Attention lors de l'association d'un nouveau podometre, celui ci n'est pas pris en compte dans le service